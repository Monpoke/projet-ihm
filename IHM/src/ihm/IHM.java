/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ihm;

import ihm.windows.Calling;
import ihm.windows.CallingList;
import ihm.windows.CriteriaList;
import ihm.windows.MainWindow;

/**
 *
 * @author Pierre
 */
public class IHM {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

       /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                MainWindow mainWindow = new MainWindow();
                mainWindow.setResizable(false);
                mainWindow.fastCalling();
                mainWindow.setVisible(true);
            }
        });
        
    }
    
}
