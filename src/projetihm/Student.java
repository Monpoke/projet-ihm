package projetihm;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Student is a class representing a person on the register.
 *
 * @author Quentin
 * @version 0.0.0
 * @since 2015-05-26
 */
public class Student {

    /**
     * The list of all students.
     */
    private static List<Student> students = new ArrayList<>();

    /**
     * The name of the student.
     */
    private String name;

    /**
     * E-mail address of the student.
     */
    private String email;

    /**
     * Phone number of the student.
     */
    private String phone;

    /**
     * Whether or not the student is present on the register.
     */
    private boolean present;

    /**
     * Whether or not the student was late.
     */
    private boolean late;

    /**
     * Whether or not the student has been registered.
     */
    private boolean registered;

    /**
     * Photo of the student.
     */
    private Image photo;

    /**
     * List of criterias for this student.
     */
    private List<Criteria> criterias;

    /**
     * Creates a new student.
     *
     * @param name  Name of the student.
     * @param photo Photo of the student.
     * @param email E-mail of the student.
     */
    public Student(String name, Image photo, String email) {
        this(name, photo, email, null);
    }

    /**
     * Creates a new student.
     *
     * @param name  Name of the student.
     * @param photo Photo of the student.
     * @param email E-mail of the student.
     * @param phone Phone number of the student.
     */
    public Student(String name, Image photo, String email, String phone) {
        this.name = name;
        this.photo = photo;
        this.email = email;
        this.phone = phone;
        this.criterias = new ArrayList<>();

        students.add(this);
    }

    /**
     * Returns the list of all the students.
     *
     * @return  List of all the students.
     */
    public static List<Student> getStudents() {
        return students;
    }

    /**
     * Returns the student of id specified.
     *
     * @param id    Id of the student.
     * @return      Student of id specified.
     */
    public static Student getStudent(int id) {
        return students.get(id);
    }

    /**
     * Returns the number of students
     *
     * @return  Number of students.
     */
    public static int getNbStudents() {
        return students.size();
    }

    /**
     * Returns the name of the student.
     *
     * @return  Name of the student.
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the e-mail of the student.
     *
     * @return  E-mail of the student.
     */
    public String getEmail() { return email; }

    /**
     * Returns the phone number of the student.
     *
     * @return  Phone number of the student.
     */
    public String getPhone() {
        return (phone != null) ? phone : "Non renseigné";
    }

    /**
     * Returns the photo of the student.
     *
     * @return  Photo of the student.
     */
    public Image getPhoto() {
        return photo;
    }

    /**
     * Returns the list of criterias of this student.
     *
     * @return  List of criterias of this student.
     */
    public List<Criteria> getCriterias() { return criterias; }

    /**
     * Adds a criteria to the student.
     *
     * @param criteria  Criteria to add.
     */
    public void addCriteria(Criteria criteria) {
        criterias.add(criteria);
        criteria.addStudent(this);
    }

    /**
     * Removes a criteria to the student.
     *
     * @param criteria  Criteria to remove.
     */
    public void removeCriteria(Criteria criteria) {
        criterias.remove(criteria);
    }

    /**
     * Returns whether or not the student is present.
     *
     * @return  True is the student is present, false if they are missing.
     */
    public boolean isPresent() {
        return present;
    }

    /**
     * Sets the student as late.
     *
     * @param value True is the student was late, false if they arrived on time.
     */
    public void setLate(boolean value) {
        this.late = value;
    }

    /**
     * Returns whether or not the student was late.
     *
     * @return  True is the student was late, false if they arrived on time.
     */
    public boolean isLate() {
        return late;
    }

    /**
     * Sets the student's presence on the register.
     *
     * @param value True is the student is present, false if they are missing.
     */
    public void setPresent(boolean value) {
        this.present = value;
        setRegistered(true);
    }

    /**
     * Returns whether or not the student was registered.
     *
     * @return  True is the student was registered, false otherwise.
     */
    public boolean isRegistered() {
        return registered;
    }

    /**
     * Sets the student as registered or not.
     *
     * @param value True if the student is registered, false otherwise.
     */
    public void setRegistered(boolean value) {
        this.registered = value;
    }

}
