package projetihm;

import projetihm.graphics.components.buttons.ToggleSwitch;
import projetihm.graphics.components.checkboxes.Checkbox;
import projetihm.graphics.components.checkboxes.CheckboxType;
import projetihm.graphics.components.images.Hamburger;

import javax.imageio.ImageIO;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import projetihm.graphics.components.images.BackButton;
import projetihm.graphics.screens.Profile;
import projetihm.graphics.screens.Settings;
import projetihm.graphics.screens.SideMenu;

/**
 * LoadResources is a utility class that loads all the data and resources of the app.
 *
 * @author Quentin
 * @version 0.0.0
 * @since 2015-05-26
 */
public class LoadResources implements Constants {

    /**
     * Loads all the resources.
     */
    public static void load() {
        try {
            // Load images.
            Hamburger.image = ImageIO.read(new File(ASSETS_PATH + "hamburger.png"));
            BackButton.image = ImageIO.read(new File(ASSETS_PATH + "back.png"));
            Checkbox.tick = ImageIO.read(new File(CHECKBOXES_PATH + "tick.png"));
            CheckboxType.DEFAULT.image = ImageIO.read(new File(CHECKBOXES_PATH + "default.png"));
            CheckboxType.GREEN.image = ImageIO.read(new File(CHECKBOXES_PATH + "green.png"));
            CheckboxType.RED.image = ImageIO.read(new File(CHECKBOXES_PATH + "red.png"));
            CheckboxType.ORANGE.image = ImageIO.read(new File(CHECKBOXES_PATH + "orange.png"));
            ToggleSwitch.onImage = loadAsset("on-switch");
            ToggleSwitch.offImage = loadAsset("off-switch");
            System.out.println("Hello world!");
            // Load side menu images
            SideMenu.registerImage = loadImage("people");
            SideMenu.quickRegisterImage = loadImage("check");
            SideMenu.lateImage = loadImage("alarm");
            SideMenu.criteriasImage = loadImage("star");
            SideMenu.settingsImage = loadImage("gear");

            // Load settings images.
            Settings.remindImage = loadImage("alarm");
            Settings.vibrateImage = loadImage("vibrate");
            Settings.soundImage = loadImage("sound");

            // Load criteria icons.
            File[] icons = new File(CRITERIAS_PATH).listFiles();
            for (File icon : icons) {
                Criteria.icons.add(ImageIO.read(icon));
            }
            Criteria.addImage = loadAsset("plus");

            // Load criterias.
            Criteria participation = new Criteria("Participation", loadImage("star"));
            Criteria bien = new Criteria("Bien", loadImage("thumbsup"));
            Criteria pasBien = new Criteria("Pas bien", loadImage("thumbsdown"));
            Criteria retardFrequent = new Criteria("Retard", loadImage("thumbsdown"));

            // Load profile icons.
            Profile.emailImage = loadAsset("email");
            Profile.phoneImage = loadAsset("phone");

            // Load students.
            new Student("Michel Gérard Joseph Colucci", loadPhoto("michel_gerard_joseph_colucci"), "coluche@aile-ou-cuisse.fr");
            new Student("Paul Durant", loadPhoto("paul_durant"), "paul.durant@etudiant.univ-lille1.fr", "(+33) 06 66 99 69 96");
            new Student("Pierre Dupond", loadPhoto("pierre_dupond"), "pierre.dupond@etudiant.univ-lille1.fr");
            new Student("Katie Paxton-Fear", loadPhoto("katie_paxton-fear"), "k.paxton-fear@edu.salford.ac.uk", "(+44) 07540 377732");
            Student pierre = new Student("Pierre Bourgeois", loadPhoto("pierre_bourgeois"), "pierre.bourgeois@etudiant.univ-lille1.fr");
            Student quentin = new Student("Quentin Brault", loadPhoto("quentin_brault"), "quentin.brault@etudiant.univ-lille1.fr", "(+33) 07 37 27 17 77");
            new Student("Jacques Richard", loadPhoto("jacques_richard"), "jacques.richard@etudiant.univ-lille1.fr");
            new Student("Jeanne Robin", loadPhoto("jeanne_robin"), "poitoucharentesbabe@free.fr");
            new Student("Claire Martin", loadPhoto("claire_martin"), "claire.martin@etudiant.univ-lille1.fr", "(+33) 02 37 12 15 16");
            pierre.addCriteria(pasBien);
            pierre.addCriteria(retardFrequent);
            quentin.addCriteria(participation);
            quentin.addCriteria(bien);

        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Loads the photo of a student.
     *
     * @param name  Name of the student.
     * @return      Photo of the student.
     * @throws IOException  If the image was not found.
     */
    private static Image loadPhoto(String name) throws IOException {
        File image;
        // If the photo exists.
        if ((image = new File(PHOTOS_PATH + name + ".png")).exists()) {
            return ImageIO.read(image);
        }
        // If the photo doesn't exist, set it as the default one.
        return ImageIO.read(new File(PHOTOS_PATH + "default_photo.png"));
    }

    /**
     * Loads the image at the specified location.
     *
     * @param path Path to the image.
     * @return Image at the specified location.
     * @throws IOException  If the image was not found.
     */
    private static Image loadImage(String path) throws IOException {
        File image;
        // If the image exists.
        if ((image = new File(IMAGES_PATH + path + ".png")).exists()) {
            return ImageIO.read(image);
        }
        // If the image doesn't exist, set it as the default one.
        return ImageIO.read(new File(IMAGES_PATH + "default_image.png"));
    }

    /**
     * Loads the resource at the specified location.
     *
     * @param path Path to the resource.
     * @return Resource at the specified location.
     * @throws IOException If the resource was not found.
     */
    private static Image loadAsset(String path) throws IOException {
        File image = new File(ASSETS_PATH + path + ".png");
        return ImageIO.read(image);
    }

}
