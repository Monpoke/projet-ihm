package projetihm;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Criteria
 *
 * @author Tititesouris
 * @version 0.0.0
 * @since 2015-06-02
 */
public class Criteria {

    /**
     * List of all the icons to pick from for an icon image.
     */
    public static List<Image> icons = new ArrayList<>();

    /**
     * Icon image to add a new criteria.
     */
    public static Image addImage;

    /**
     * List of all the criterias created.
     */
    private static List<Criteria> criterias = new ArrayList<>();

    /**
     * List of the students having that criteria.
     */
    private List<Student> students;

    /**
     * Name of the criteria.
     */
    private String name;

    /**
     * Image of the criteria.
     */
    private Image image;

    /**
     * Creates a new criteria.
     *
     * @param name  Name of the criteria.
     */
    public Criteria(String name) {
        this(name, null);
        this.image = icons.get(0);
    }

    /**
     * Creates a new criteria.
     *
     * @param name  Name of the criteria.
     * @param image Image of the criteria
     */
    public Criteria(String name, Image image) {
        this.name = name;
        this.image = image;
        criterias.add(this);
        students = new ArrayList<>();
    }

    /**
     * Removes the specified criteria.
     *
     * @param criteria  Criteria to remove.
     */
    public static void remove(Criteria criteria) {
        criterias.remove(criteria);
    }

    /**
     * Returns the name of the criteria.
     *
     * @return  Name of the criteria.
     */
    public String getName() { return name; }

    /**
     * Sets the name of the criteria to the one specified.
     *
     * @param name  New name of the criteria.
     */
    public void setName(String name) { this.name = name; }

    /**
     * Returns the image of the criteria.
     *
     * @return  Image of the criteria.
     */
    public Image getImage() { return image; }

    /**
     * Sets the criteria image to the one specified.
     *
     * @param image Image of the criteria.
     */
    public void setImage(Image image) { this.image = image; }

    /**
     * Adds a student to the list of students using this criteria.
     *
     * @param student   Student using that criteria.
     */
    public void addStudent(Student student) {
        students.add(student);
    }

    /**
     * Removes a student from the list of students using that criteria.
     *
     * @param student   Student no longer using that criteria.
     */
    public void removeStudent(Student student) {
        students.remove(student);
    }

    /**
     * Returns the list of all students using that criteria.
     *
     * @return  List of student using this criteria.
     */
    public List<Student> getStudents() { return students; }

    /**
     * Returns the number of criterias.
     *
     * @return  Number of criterias.
     */
    public static int getNbCriterias() { return criterias.size(); }

    /**
     * Returns the criteria of id specified.
     *
     * @param id    Id of the criteria.
     */
    public static Criteria getCriteria(int id) { return criterias.get(id); }

    /**
     * Returns the list of criterias.
     *
     * @return List of criterias.
     */
    public static List<Criteria> getCriterias() {
        return criterias;
    }

}
