package projetihm.graphics.screens;

import projetihm.Student;
import projetihm.graphics.Fonts;
import projetihm.graphics.components.texts.LabelPanel;
import projetihm.listeners.SwipeListener;
import projetihm.listeners.TouchListener;
import projetihm.graphics.components.images.ImageBox;
import projetihm.graphics.components.texts.Label;
import projetihm.graphics.components.composites.TopBar;
import projetihm.graphics.components.buttons.Button;
import projetihm.graphics.components.buttons.ButtonType;

import java.awt.event.MouseEvent;

/**
 * QuickRegister is a screen allowing the user to quickly go through the register.
 *
 * @author Quentin
 * @version 0.0.0
 * @since 2015-05-25
 */
public class QuickRegister extends Screen {

    /**
     * Padding in pixels between elements.
     */
    public static final int PADDING = 10;

    /**
     * Width and height of the profile picture.
     */
    public static final int IMAGE_SIZE = SCREEN_WIDTH / 2;

    /**
     * Width of the buttons.
     */
    public static final int BUTTON_WIDTH = 2 * SCREEN_WIDTH / 3;

    /**
     * Height of the buttons.
     */
    public static final int BUTTON_HEIGHT = 80;

    /**
     * Creates a new QuickRegister screen.
     */
    public QuickRegister() {
        this(0);
    }

    /**
     * Creates a new QuickRegister screen displaying the student of id specified.
     *
     * @param studentId Id of the student on the screen.
     */
    public QuickRegister(int studentId) {
        super("Appel Rapide");

        Student student = Student.getStudent(studentId);

        // Add the top bar.
        add(new TopBar(TOPBAR_HEIGHT, "N2P2 - Groupe I - BDD"));

        // Add the profile picture.
        add(new ImageBox(student.getPhoto(), (SCREEN_WIDTH - IMAGE_SIZE) / 2, TOPBAR_HEIGHT + PADDING, IMAGE_SIZE, IMAGE_SIZE, true, new TouchListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
                changeScreen(new Profile(student));
            }
        }));

        // Add the student name.
        add(new LabelPanel(0, TOPBAR_HEIGHT + IMAGE_SIZE + PADDING, SCREEN_WIDTH, 50, new Label(student.getName(), Fonts.SMALL_FONT)));

        // Create the buttons.
        Button present = new Button("Présent", (SCREEN_WIDTH - BUTTON_WIDTH) / 2, SCREEN_HEIGHT - 2 * (BUTTON_HEIGHT + PADDING), BUTTON_WIDTH, BUTTON_HEIGHT, Fonts.BIG_FONT, ButtonType.GREEN);
        Button missing = new Button("Absent", (SCREEN_WIDTH - BUTTON_WIDTH) / 2, SCREEN_HEIGHT - (BUTTON_HEIGHT + PADDING), BUTTON_WIDTH, BUTTON_HEIGHT, Fonts.BIG_FONT, ButtonType.RED);

        // Check if this student has already been registered.
        if (student.isRegistered()) {
            if (student.isPresent()) {
                present.setSelected();
            } else {
                missing.setSelected();
            }
        }

        // Add button event handlers.
        present.addTouchListener(new TouchListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
                student.setPresent(true);
                if (studentId < Student.getNbStudents() - 1) {
                    changeScreen(new QuickRegister(studentId + 1));
                } else {
                    changeScreen(new Register());
                }
            }
        });

        missing.addTouchListener(new TouchListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
                student.setPresent(false);
                if (studentId < Student.getNbStudents() - 1) {
                    changeScreen(new QuickRegister(studentId + 1));
                } else {
                    changeScreen(new Register());
                }
            }
        });

        // Add the buttons.
        add(present);
        add(missing);

        // Add swipe event handler.
        SwipeListener swipeListener = new SwipeListener() {
            @Override
            public void swipeUp(int distance) {

            }

            @Override
            public void swipeDown(int distance) {

            }

            @Override
            public void swipeLeft(int distance) {
                if (studentId < Student.getNbStudents() - 1) {
                    changeScreen(new QuickRegister(studentId + 1));
                }
            }

            @Override
            public void swipeRight(int distance) {
                if (studentId > 0) {
                    changeScreen(new QuickRegister(studentId - 1), SWITCH_RIGHT);
                }
            }
        };
        // Add the listener to every component on the screen.
        addMouseListener(swipeListener);
        for (java.awt.Component component : getComponents()) {
            component.addMouseListener(swipeListener);
        }

    }

}
