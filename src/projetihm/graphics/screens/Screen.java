package projetihm.graphics.screens;

import projetihm.graphics.components.Panel;

/**
 * Screen is the container containing every graphical component. It is contained
 * withing a ScreenPanel to allow switching from one screen to another.
 *
 * @see projetihm.graphics.screens.ScreenPanel
 *
 * @author Quentin
 * @version 0.0.0
 * @since 2015-05-25
 */
public abstract class Screen extends Panel {

    /**
     * The ScreenPanel containing this screen.
     */
    private ScreenPanel screenPanel;

    /**
     * Whether or not the ScreenPanel is currently switching screen.
     */
    protected boolean switching;

    /**
     * Creates a new screen.
     *
     * @param name Name of the screen.
     */
    public Screen(String name) {
        super(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        setName(name);
    }

    /**
     * Changes the screen to display.
     *
     * @param screen New screen to display.
     */
    public void changeScreen(Screen screen) {
        if (!switching) {
            screenPanel.changeScreen(screen, SWITCH_LEFT, 10, 100);
        }
    }

    /**
     * Changes the screen to display.
     *
     * @param screen New screen to display.
     * @param direction Direction of exit of the current screen.
     */
    public void changeScreen(Screen screen, int direction) {
        if (!switching) {
            screenPanel.changeScreen(screen, direction, 10, 100);
        }
    }

    /**
     * Changes the screen to display.
     *
     * @param screen New screen to display.
     * @param direction Direction of exit of the current screen.
     * @param speed Speed of exit in pixels per update.
     */
    public void changeScreen(Screen screen, int direction, int speed) {
        if (!switching) {
            screenPanel.changeScreen(screen, direction, speed, 100);
        }
    }

    /**
     * Changes the screen to display.
     *
     * @param screen New screen to display.
     * @param direction Direction of exit of the current screen.
     * @param speed Speed of exit in pixels per update.
     * @param percentage Percentage of the window with for apparition
     */
    public void changeScreen(Screen screen, int direction, int speed, int percentage) {
        if (!switching) {
            screenPanel.changeScreen(screen, direction, speed, percentage);
        }
    }

    /**
     * Sets the screen panel to the one specified.
     *
     * @param screenPanel Screen panel containing the screen.
     */
    public void setScreenPanel(ScreenPanel screenPanel) {
        this.screenPanel = screenPanel;
    }

    /**
     * Returns the screen panel.
     *
     * @return Screen panel
     */
    public ScreenPanel getScreenPanel() {
        return screenPanel;
    }

    /**
     * Sets whether or not the screen is being switched.
     *
     * @param value True if the screen is switching, false otherwise.
     */
    public void setSwitching(boolean value) {
        switching = value;
    }

    @Override
    public String toString() {
        return getName() + " [" + getX() + ", " + getY() + ", " + getWidth() + ", " + getHeight() + "]";
    }

}
