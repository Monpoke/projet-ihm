package projetihm.graphics.screens;

import projetihm.Criteria;
import projetihm.graphics.Fonts;
import projetihm.graphics.components.buttons.Button;
import projetihm.graphics.components.buttons.ButtonType;
import projetihm.graphics.components.composites.*;
import projetihm.graphics.components.lists.List;
import projetihm.graphics.components.lists.ListItem;
import projetihm.graphics.components.lists.ListPanel;
import projetihm.listeners.TouchListener;

import java.awt.event.MouseEvent;

/**
 * Criterias
 *
 * @author Tititesouris
 * @version 0.0.0
 * @since 2015-06-02
 */
public class Criterias extends Screen {

    /**
     * Height of the button.
     */
    private static final int BUTTON_HEIGHT = 50;

    /**
     * Creates a new Criterias screen.
     */
    public Criterias() {
        super("Critères");

        add(new TopBar(TOPBAR_HEIGHT, "Critères"));

        // Add the criteria list.
        ListItem[] items = new CriteriaListItem[Criteria.getNbCriterias()];
        for (int i = 0; i < Criteria.getNbCriterias(); i++) {
            items[i] = new CriteriaListItem(Criteria.getCriteria(i));
        }
        List criteriaList = new CriteriaList(items);
        add(new ListPanel(0, TOPBAR_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT - TOPBAR_HEIGHT - BUTTON_HEIGHT, criteriaList));


        Button newCriteria = new Button("Nouveau critère", 0, SCREEN_HEIGHT - BUTTON_HEIGHT, SCREEN_WIDTH, BUTTON_HEIGHT, Fonts.MEDIUM_FONT, ButtonType.GREEN);

        newCriteria.addTouchListener(new TouchListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
                changeScreen(new projetihm.graphics.screens.Criteria());
            }
        });
        // Add the button.
        add(newCriteria);

    }

}
