package projetihm.graphics.screens;

import projetihm.graphics.components.composites.MenuList;
import projetihm.graphics.components.composites.MenuListItem;

import projetihm.graphics.components.composites.TopBar;
import projetihm.graphics.components.lists.List;
import projetihm.graphics.components.lists.ListItem;
import projetihm.graphics.components.lists.ListPanel;

import java.awt.*;

/**
 * SideMenu
 *
 * @author Pierre
 * @version 0.0.0
 * @since 2015-05-25
 */
public class SideMenu extends Screen {

    /**
     * Image by the side of the Register option.
     */
    public static Image registerImage;

    /**
     * Image by the side of the Quick Register option.
     */
    public static Image quickRegisterImage;

    /**
     * Image by the side of the Late option.
     */
    public static Image lateImage;

    /**
     * Image by the side of the Criterias option.
     */
    public static Image criteriasImage;

    /**
     * Image by the side of the Settings option.
     */
    public static Image settingsImage;

    /**
     * Height of the button.
     */
    private static final int BUTTON_HEIGHT = 50;

    /**
     * Creates a new sidemenu
     */
    public SideMenu() {
        super("Menu");
        setBackground(Color.white);
        
        // Add top bar
        add(new TopBar(TOPBAR_HEIGHT, "Menu", true));

        ListItem[] items = new ListItem[]{
                new MenuListItem("Appel", new Register(), registerImage),
                new MenuListItem("Appel Rapide", new QuickRegister(), quickRegisterImage),
                new MenuListItem("Retards", new Late(), lateImage),
                new MenuListItem("Critères", new Criterias(), criteriasImage),
                new MenuListItem("Options", new Settings(), settingsImage)
        };
        
        List menuList = new MenuList(items);
        add(new ListPanel(0, TOPBAR_HEIGHT + 20, SCREEN_WIDTH, SCREEN_HEIGHT - TOPBAR_HEIGHT - BUTTON_HEIGHT, menuList));

    }

}
