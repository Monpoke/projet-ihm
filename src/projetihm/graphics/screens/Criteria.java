package projetihm.graphics.screens;

import projetihm.Student;
import projetihm.graphics.Fonts;
import projetihm.graphics.components.buttons.Button;
import projetihm.graphics.components.buttons.ButtonType;
import projetihm.graphics.components.composites.ImagePopup;
import projetihm.graphics.components.composites.TopBar;
import projetihm.graphics.components.images.ImageBox;
import projetihm.graphics.components.texts.InputBox;
import projetihm.listeners.TouchListener;

import java.awt.*;
import java.awt.event.MouseEvent;

/**
 * Criteria
 *
 * @author Tititesouris
 * @version 0.0.0
 * @since 2015-06-02
 */
public class Criteria extends Screen {

    /**
     * Padding in pixels between elements.
     */
    private static final int PADDING = 10;

    /**
     * Width and height of the criteria image.
     */
    private static final int IMAGE_SIZE = SCREEN_WIDTH / 2;

    /**
     * Width of the criteria name.
     */
    private static final int NAME_WIDTH = 3 * SCREEN_WIDTH / 4;

    /**
     * Height of the buttons.
     */
    private static final int BUTTON_HEIGHT = 80;

    /**
     * Creates a new Criteria screen.
     */
    public Criteria() {
        this(new projetihm.Criteria("Nouveau critère"));
    }

    /**
     * Creates a new Criteria screen.
     *
     * @param criteria  Criteria being modified.
     */
    public Criteria(projetihm.Criteria criteria) {
        super(criteria.getName());

        // Add the top bar.
        add(new TopBar(TOPBAR_HEIGHT, "Modifier un critère", true));

        // Add the icon picture.
        ImageBox criteriaImage = new ImageBox(criteria.getImage(), (SCREEN_WIDTH - IMAGE_SIZE) / 2, TOPBAR_HEIGHT + PADDING, IMAGE_SIZE, IMAGE_SIZE, true);

        // Add the image picker popup
        Image[] icons = new Image[projetihm.Criteria.icons.size()];
        for (int i = 0; i < icons.length; i++) {
            icons[i] = projetihm.Criteria.icons.get(i);
        }
        ImagePopup imagePopup = new ImagePopup(PADDING, TOPBAR_HEIGHT + PADDING, SCREEN_WIDTH - 2 * PADDING, SCREEN_WIDTH - 2 * PADDING, icons, 3);
        imagePopup.addTouchListener(new TouchListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
                // Update the image of the criteria.
                criteria.setImage(imagePopup.getSelected());
                // Update the image of the ImageBox.
                criteriaImage.setImage(imagePopup.getSelected().getScaledInstance(criteriaImage.getWidth(), criteriaImage.getHeight(), Image.SCALE_SMOOTH));
            }
        });
        add(imagePopup);

        criteriaImage.addTouchListener(new TouchListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
                imagePopup.setVisible(true);
            }
        });
        add(criteriaImage);

        // Add the criteria name.
        InputBox criteriaName = new InputBox((SCREEN_WIDTH - NAME_WIDTH) / 2, TOPBAR_HEIGHT + IMAGE_SIZE + 2 * PADDING, NAME_WIDTH, 50, criteria.getName());
        add(criteriaName);

        Button delete = new Button("Supprimer", 0, SCREEN_HEIGHT - BUTTON_HEIGHT, SCREEN_WIDTH / 2, BUTTON_HEIGHT, Fonts.MEDIUM_FONT, ButtonType.RED);
        Button confirm = new Button("Valider", SCREEN_WIDTH / 2, SCREEN_HEIGHT - BUTTON_HEIGHT, SCREEN_WIDTH / 2, BUTTON_HEIGHT, Fonts.MEDIUM_FONT, ButtonType.GREEN);

        // Add button event handlers.
        delete.addTouchListener(new TouchListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
                projetihm.Criteria.remove(criteria);
                for (Student student : criteria.getStudents()) {
                    student.removeCriteria(criteria);
                }
                changeScreen(new Criterias(), SWITCH_RIGHT);
            }
        });

        confirm.addTouchListener(new TouchListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
                criteria.setName(criteriaName.getText());
                changeScreen(new Criterias(), SWITCH_RIGHT);
            }
        });

        add(delete);
        add(confirm);

    }

}
