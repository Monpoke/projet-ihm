package projetihm.graphics.screens;

import projetihm.Constants;
import projetihm.Debug;
import projetihm.graphics.components.Component;
import projetihm.graphics.components.Window;

import javax.swing.*;
import java.awt.*;

/**
 * ScreenPanel is the second upper-most container. It is contained in a Window
 * and contains the screen to display. It allows switching from one screen to
 * another.
 *
 * @see projetihm.graphics.components.Window
 * @see projetihm.graphics.screens.Screen
 *
 * @author Quentin
 * @version 0.0.0
 * @since 2015-05-30
 */
public class ScreenPanel extends JPanel implements Constants, Component {

    /**
     * Window containing this screen panel.
     */
    private Window window;

    /**
     * Screen to display.
     */
    private Screen screen;

    /**
     * Next screen to display.
     */
    private Screen nextScreen;
    
    /**
     * Save a reference to last screen
     */
    private Screen lastScreen;

    /**
     * Direction of exit of the current screen, and the direction of entrance of
     * the next screen.
     */
    private int switchDirection;

    /**
     * Speed at which the panel switches from one screen to another.
     */
    private int switchSpeed;

    /**
     * Width percentage of the window width for apparition.
     */
    private int switchPercentage;
    
    /**
     * Save the last switch direction
     */
    private int switchLastSwitch;

    /**
     * Creates a new screen panel.
     *
     * @param window Window containing the screen panel.
     * @param screen Screen contained in the screen panel.
     */
    public ScreenPanel(Window window, Screen screen) {
        this.window = window;

        // Add the screen.
        add(screen);
        setScreen(screen);

        // We'll use our own layout.
        setLayout(null);

        // Set smartphone size.
        setPreferredSize(new Dimension(SCREEN_WIDTH, SCREEN_HEIGHT));

        // Background color of the screen.
        setBackground(Color.white);
    }

    /**
     * Changes the screen to display.
     *
     * @param screen New screen to display.
     * @param direction Direction of exit of the current screen.
     * @param speed Speed of exit in pixels per update.
     * @param percentage Percentage of window width.
     */
    public void changeScreen(Screen screen, int direction, int speed, int percentage) {
        // Update the screens information.
        this.screen.setSwitching(true);
        screen.setSwitching(true);

        // Update the direction and speed.
        this.switchDirection = direction;
        this.switchSpeed = speed;
        this.switchPercentage = percentage;

        // Add the new screen.
        this.nextScreen = screen;
        this.nextScreen.setLocation(-switchDirection * screen.getWidth(), screen.getY());
        add(this.nextScreen);

    }

    /**
     * Sets the current screen to the one specified.
     *
     * @param nextScreen New screen to display.
     */
    public void setScreen(Screen nextScreen) {
        // Remove the old screen.
        if (screen != null) {
            remove(screen);
            lastScreen = screen;
            switchLastSwitch = switchDirection;
        }

        // Keep a reference to the new screen.
        screen = nextScreen;

        // Update the new screen's information.
        screen.setScreenPanel(this);
        screen.setSwitching(false);

        // Set the title of the window.
        window.setTitle(screen.getName());

        // Repaint the screen.
        screen.repaint();

        Debug.showTree(window);

    }

    /**
     * Return on the last screen
     */
    public void returnOnLastScreen() {
        if (lastScreen != null) {
            changeScreen(lastScreen, -this.switchLastSwitch, 10, 100);
        }
    }
    
    @Override
    public String toString() {
        return "ScreenPanel [" + getX() + ", " + getY() + ", " + getWidth() + ", " + getHeight() + "]";
    }

    @Override
    public void update() {
        if (screen != null) {

            // If there's a screen to switch to.
            if (nextScreen != null) {
                // If it is switching from a screen to another.
                if (screen != nextScreen) {

                    // Break point
                    int breakPoint = (int) ((this.switchPercentage / 100.0) * SCREEN_WIDTH);
                    // If the screen is still visible.
                    if (switchDirection == SWITCH_LEFT && (nextScreen.getX()) > 0
                            || switchDirection == SWITCH_RIGHT && screen.getX() < breakPoint) {
                        // Move the screen out of view.
                        screen.setLocation(screen.getX() + switchDirection * switchSpeed, screen.getY());
                        nextScreen.setLocation(nextScreen.getX() + switchDirection * switchSpeed, nextScreen.getY());

                    } // If the screen is no longer visible.
                    else {
                        // Set the new screen.
                        setScreen(nextScreen);
                    }

                }
            }

            screen.update();
        }
    }

}
