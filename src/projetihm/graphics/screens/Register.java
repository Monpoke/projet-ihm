package projetihm.graphics.screens;

import projetihm.Student;
import projetihm.graphics.Fonts;
import projetihm.graphics.components.buttons.ButtonType;
import projetihm.graphics.components.composites.StudentList;
import projetihm.graphics.components.lists.List;
import projetihm.graphics.components.lists.ListItem;
import projetihm.graphics.components.composites.StudentListItem;
import projetihm.graphics.components.composites.TopBar;
import projetihm.graphics.components.buttons.Button;
import projetihm.graphics.components.lists.ListPanel;
import projetihm.listeners.SwipeListener;
import projetihm.listeners.TouchListener;

import java.awt.event.MouseEvent;

/**
 * Register is a screen allowing the user to see the whole register at once and select which students are present or missing.
 *
 * @author Quentin
 * @version 0.0.0
 * @since 2015-05-25
 */
public class Register extends Screen {

    /**
     * Height of the button.
     */
    private static final int BUTTON_HEIGHT = 50;

    /**
     * Creates a new Register screen.
     */
    public Register() {
        super("Appel");

        // Add the top bar.
        add(new TopBar(TOPBAR_HEIGHT, "N2P2 - Groupe I - BDD"));

        // Add the student list.
        ListItem[] items = new StudentListItem[Student.getNbStudents()];
        for (int i = 0; i < Student.getNbStudents(); i++) {
            items[i] = new StudentListItem(Student.getStudent(i));
        }
        List studentList = new StudentList(items);
        add(new ListPanel(0, TOPBAR_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT - TOPBAR_HEIGHT - BUTTON_HEIGHT, studentList));

        // Add swipe event handler.
        SwipeListener swipeListener = new SwipeListener() {
            @Override
            public void swipeUp(int distance) {
                studentList.scroll(-distance / 2);
            }

            @Override
            public void swipeDown(int distance) {
                studentList.scroll(distance / 2);
            }

            @Override
            public void swipeLeft(int distance) {

            }

            @Override
            public void swipeRight(int distance) {

            }
        };
        // Add the listener to every component on the screen.
        addMouseListener(swipeListener);
        for (ListItem item : studentList.getItems()) {
            ((StudentListItem)item).addSwipeListener(swipeListener);
        }

        Button absents = new Button("Tous absents", 0, SCREEN_HEIGHT - BUTTON_HEIGHT, SCREEN_WIDTH / 2, BUTTON_HEIGHT, Fonts.SMALL_FONT, ButtonType.RED);
        Button presents = new Button("Tous présents", SCREEN_WIDTH / 2, SCREEN_HEIGHT - BUTTON_HEIGHT, SCREEN_WIDTH / 2, BUTTON_HEIGHT, Fonts.SMALL_FONT, ButtonType.GREEN);

        absents.addTouchListener(new TouchListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
                ((StudentList) studentList).setStudentsPresent(false);
            }
        });
        presents.addTouchListener(new TouchListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
                ((StudentList) studentList).setStudentsPresent(true);
            }
        });

        // Add the buttons.
        add(absents);
        add(presents);

    }

}
