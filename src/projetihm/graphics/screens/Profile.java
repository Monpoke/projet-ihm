package projetihm.graphics.screens;

import projetihm.*;
import projetihm.Criteria;
import projetihm.graphics.Fonts;
import projetihm.graphics.components.composites.TopBar;
import projetihm.graphics.components.images.ImageBox;
import projetihm.graphics.components.texts.Label;
import projetihm.graphics.components.texts.LabelPanel;
import projetihm.listeners.TouchListener;

import java.awt.*;
import java.awt.event.MouseEvent;

/**
 * Profile
 *
 * @author Tititesouris
 * @version 0.0.0
 * @since 2015-06-01
 */
public class Profile extends Screen {

    /**
     * Image of the e-mail icon.
     */
    public static Image emailImage;

    /**
     * Image of the phone icon.
     */
    public static Image phoneImage;

    /**
     * Padding in pixels between elements.
     */
    private static final int PADDING = 10;

    /**
     * Width and height of the profile picture.
     */
    private static final int IMAGE_SIZE = SCREEN_WIDTH / 2;

    /**
     * Width and height of the criteria icons.
     */
    private static final int ICON_SIZE = 32;

    /**
     * Height of the text.
     */
    private static final int TEXT_SIZE = 35;

    /**
     * Creates a new Profile screen.
     *
     * @param student   Student who owns this profile.
     */
    public Profile(Student student) {
        super(student.getName());

        add(new TopBar(TOPBAR_HEIGHT, "N2P2 - Groupe I - BDD", true));

        // Add the profile picture.
        add(new ImageBox(student.getPhoto(), (SCREEN_WIDTH - IMAGE_SIZE) / 2, TOPBAR_HEIGHT + PADDING, IMAGE_SIZE, IMAGE_SIZE, true));

        // Add the student's criterias.
        int x = (SCREEN_WIDTH - student.getCriterias().size() * (ICON_SIZE + 5)) / 2;
        int y = TOPBAR_HEIGHT + IMAGE_SIZE + PADDING + 5;
        int i = 0;
        for (Criteria criteria : student.getCriterias()) {
            add(new ImageBox(criteria.getImage(), x + i * (ICON_SIZE + 5), y, ICON_SIZE, ICON_SIZE));
            i++;
        }
        // Add the button to add a criteria.
        add(new ImageBox(Criteria.addImage, SCREEN_WIDTH - ICON_SIZE - PADDING, y, ICON_SIZE, ICON_SIZE, false, new TouchListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
                Criteria newCriteria = Criteria.getCriteria(0);
                add(new ImageBox(newCriteria.getImage(), x + student.getCriterias().size() * (ICON_SIZE + 5), y, ICON_SIZE, ICON_SIZE));
                student.addCriteria(newCriteria);
                repaint();
            }
        }));

        // Add the student's name.
        add(new LabelPanel(0, TOPBAR_HEIGHT + IMAGE_SIZE + ICON_SIZE + 2 * PADDING, SCREEN_WIDTH, TEXT_SIZE, new Label(student.getName())));

        // Add the student's e-mail address.
        add(new ImageBox(emailImage, 5, SCREEN_HEIGHT - 50 - 2 * (TEXT_SIZE + PADDING), ICON_SIZE, ICON_SIZE));
        add(new LabelPanel(ICON_SIZE + 5, SCREEN_HEIGHT - 50 - 2 * (TEXT_SIZE + PADDING), SCREEN_WIDTH - ICON_SIZE - 5, TEXT_SIZE, new Label(student.getEmail(), Fonts.TINY_FONT)));

        // Add the student's phone number.
        add(new ImageBox(phoneImage, 5, SCREEN_HEIGHT - 50 - (TEXT_SIZE + PADDING), ICON_SIZE, ICON_SIZE));
        add(new LabelPanel(ICON_SIZE + 5, SCREEN_HEIGHT - 50 - (TEXT_SIZE + PADDING), SCREEN_WIDTH - ICON_SIZE - 5, TEXT_SIZE, new Label(student.getPhone(), Fonts.SMALL_FONT)));
    }

}
