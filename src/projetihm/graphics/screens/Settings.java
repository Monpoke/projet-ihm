package projetihm.graphics.screens;

import projetihm.graphics.Fonts;
import projetihm.graphics.components.buttons.ToggleSwitch;
import projetihm.graphics.components.composites.TopBar;
import projetihm.graphics.components.images.ImageBox;
import projetihm.graphics.components.texts.*;
import projetihm.graphics.components.texts.Label;

import javax.swing.*;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;

/**
 * Settings
 *
 * @author Tititesouris
 * @version 0.0.0
 * @since 2015-06-01
 */
public class Settings extends Screen {

    /**
     * Image next to the remind me setting.
     */
    public static Image remindImage;

    /**
     * Image next to the vibrate setting.
     */
    public static Image vibrateImage;

    /**
     * Image next to the sound setting.
     */
    public static Image soundImage;

    /**
     * Whether or not to remind the user to register the students.
     */
    private static boolean remind = true;

    /**
     * Minutes to wait after class starts to send a notification if the students haven't been registered.
     */
    private static int nbMinutes = 15;

    /**
     * Whether or not to vibrate when sending a notification.
     */
    private static boolean vibrate = true;

    /**
     * Whether or not to play a sound when sending a notification.
     */
    private static boolean sounds;

    /**
     * Size of the padding in pixels.
     */
    private static final int PADDING = 10;

    /**
     * Width and height of the images.
     */
    private static final int IMAGE_SIZE = 40;

    /**
     * Creates a new Settings screen.
     */
    public Settings() {
        super("Options");

        add(new TopBar(TOPBAR_HEIGHT, "Options"));

        // Time before reminder.
        InputBox time = new InputBox(PADDING, TOPBAR_HEIGHT + IMAGE_SIZE + 2 * PADDING, IMAGE_SIZE + PADDING, IMAGE_SIZE - PADDING, "" + nbMinutes);
        Label remindMinutes = new Label("minutes après le début du cours", Fonts.TINY_FONT);

        // Remind to register.
        add(new ImageBox(remindImage, PADDING, TOPBAR_HEIGHT + PADDING, IMAGE_SIZE, IMAGE_SIZE));
        add(new LabelPanel(IMAGE_SIZE + PADDING, TOPBAR_HEIGHT + PADDING, SCREEN_WIDTH - 2 * (IMAGE_SIZE + 2 * PADDING), IMAGE_SIZE, new Label("Me rappeler de faire l'appel", Fonts.TINY_FONT)));
        ToggleSwitch remindToggle = new ToggleSwitch(SCREEN_WIDTH - IMAGE_SIZE - PADDING, TOPBAR_HEIGHT + PADDING, IMAGE_SIZE, IMAGE_SIZE, remind);
        remindToggle.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                remind = e.getStateChange() == ItemEvent.SELECTED;
                if (remind) {
                    remindMinutes.setForeground(Color.black);
                } else {
                    remindMinutes.setForeground(Color.gray);
                }
                time.setEditable(remind);
            }
        });
        add(remindToggle);

        time.setEditable(remind);
        time.addCaretListener(new CaretListener() {
            @Override
            public void caretUpdate(CaretEvent e) {
                try {
                    int input = Integer.valueOf(time.getText());
                    if (0 <= input && input <= 60) {
                        time.setBorder(BorderFactory.createLineBorder(Color.green));
                    } else {
                        time.setBorder(BorderFactory.createLineBorder(Color.orange));
                    }
                    nbMinutes = Math.max(0, Math.min(60, input));
                } catch (NumberFormatException ex) {
                    time.setBorder(BorderFactory.createLineBorder(Color.red));
                }
            }
        });
        add(time);
        add(new LabelPanel(IMAGE_SIZE + PADDING, TOPBAR_HEIGHT + IMAGE_SIZE + 2 * PADDING, SCREEN_WIDTH - IMAGE_SIZE - PADDING, IMAGE_SIZE, remindMinutes));

        // Vibrations.
        add(new ImageBox(vibrateImage, PADDING, TOPBAR_HEIGHT + 2 * (IMAGE_SIZE + PADDING), IMAGE_SIZE, IMAGE_SIZE));
        add(new LabelPanel(IMAGE_SIZE + 2 * PADDING, TOPBAR_HEIGHT + 2 * (IMAGE_SIZE + PADDING), SCREEN_WIDTH - 2 * (IMAGE_SIZE + 2 * PADDING), IMAGE_SIZE, new Label("Vibrer pour rappeler", Fonts.TINY_FONT)));
        ToggleSwitch vibrateToggle = new ToggleSwitch(SCREEN_WIDTH - IMAGE_SIZE - PADDING, TOPBAR_HEIGHT + 2 * (IMAGE_SIZE + PADDING), IMAGE_SIZE, IMAGE_SIZE, vibrate);
        vibrateToggle.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                vibrate = e.getStateChange() == ItemEvent.SELECTED;
            }
        });
        add(vibrateToggle);

        // Sounds.
        add(new ImageBox(soundImage, PADDING, TOPBAR_HEIGHT + 3 * (IMAGE_SIZE + PADDING), IMAGE_SIZE, IMAGE_SIZE));
        add(new LabelPanel(IMAGE_SIZE + 2 * PADDING, TOPBAR_HEIGHT + 3 * (IMAGE_SIZE + PADDING), SCREEN_WIDTH - 2 * (IMAGE_SIZE + 2 * PADDING), IMAGE_SIZE, new Label("Jouer un son pour rappeler", Fonts.TINY_FONT)));
        ToggleSwitch soundToggle = new ToggleSwitch(SCREEN_WIDTH - IMAGE_SIZE - PADDING, TOPBAR_HEIGHT + 3 * (IMAGE_SIZE + PADDING), IMAGE_SIZE, IMAGE_SIZE, sounds);
        soundToggle.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                sounds = e.getStateChange() == ItemEvent.SELECTED;
            }
        });
        add(soundToggle);

    }

}
