package projetihm.graphics;

import java.awt.*;

/**
 * Font is an interface regrouping all the fonts of the app.
 *
 * @author Quentin
 * @version 0.0.0
 * @since 2015-05-26
 */
public interface Fonts {

    /**
     * An tiny font.
     */
    Font TINY_FONT = new Font("TinyFont", Font.PLAIN, 14);

    /**
     * A small font.
     */
    Font SMALL_FONT = new Font("SmallFont", Font.PLAIN, 20);

    /**
     * A medium font.
     */
    Font MEDIUM_FONT = new Font("MediumFont", Font.BOLD, 30);

    /**
     * A big font.
     */
    Font BIG_FONT = new Font("BigFont", Font.BOLD, 50);

}
