package projetihm.graphics.components;

import projetihm.Constants;

import javax.swing.*;

/**
 * Panel is a container that can contain components and other Panels.
 *
 * @see projetihm.graphics.components.Component
 *
 * @author Quentin
 * @version 0.0.0
 * @since 2015-05-26
 */
public class Panel extends JPanel implements Constants, Component {

    /**
     * Creates a new panel.
     *
     * @param x X coordinate of the panel.
     * @param y Y coordinate of the panel.
     * @param width Width of the panel.
     * @param height Height of the panel.
     */
    public Panel(int x, int y, int width, int height) {
        setBounds(x, y, width, height);

        setLayout(null);

        setVisible(true);
    }

    /**
     * Updates the panel and every component contained in it.
     */
    public void update() {
        for (java.awt.Component component : getComponents()) {
            if (component instanceof Component) {
                ((Component) component).update();
            } else {
                component.repaint();
            }
        }
    }

}
