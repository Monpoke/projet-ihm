package projetihm.graphics.components.images;

import projetihm.Constants;
import projetihm.graphics.components.images.ImageBox;
import projetihm.listeners.TouchListener;

import java.awt.*;
import java.awt.event.MouseEvent;
import projetihm.graphics.screens.Screen;
import projetihm.graphics.screens.ScreenPanel;
import projetihm.graphics.screens.SideMenu;

/**
 * Hamburger is a clickable image used commonly in apps to open a menu.
 *
 * @author Quentin
 * @version 0.0.0
 * @since 2015-05-26
 */
public class Hamburger extends ImageBox implements Constants, projetihm.graphics.components.Component {

    /**
     * The image of the hamburger.
     */
    public static Image image;

    /**
     * Creates a new hamburger.
     *
     * @param size Width and height of the hamburger.
     */
    public Hamburger(int size) {
        super(image, 0, 0, size, size, false);
        
        // add a new listener.
        addTouchListener(new TouchListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
                ((Screen)getParent().getParent()).changeScreen(new SideMenu(), SWITCH_RIGHT, 10, 100);
            }
        });

    }

    @Override
    public String toString() {
        return "Hamburger -> " + super.toString();
    }

}
