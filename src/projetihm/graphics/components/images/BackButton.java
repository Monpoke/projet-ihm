package projetihm.graphics.components.images;

import projetihm.Constants;
import projetihm.graphics.components.images.ImageBox;
import projetihm.listeners.TouchListener;

import java.awt.*;
import java.awt.event.MouseEvent;
import projetihm.graphics.screens.Screen;
import projetihm.graphics.screens.ScreenPanel;
import projetihm.graphics.screens.SideMenu;

/**
 * Back button is a clickable image used commonly in apps to return to last window.
 *
 * @author Pierre
 * @version 0.0.0
 * @since 2015-05-26
 */
public class BackButton extends ImageBox implements Constants, projetihm.graphics.components.Component {

    /**
     * The image of the back button.
     */
    public static Image image;

    /**
     * Creates a new hamburger.
     *
     * @param size Width and height of the back button.
     */
    public BackButton(int size) {
        super(image, 0, 0, size, size, false);
        
        // add a new listener.
        addTouchListener(new TouchListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
                ((Screen) getParent().getParent()).getScreenPanel().returnOnLastScreen();
            }
        });

    }

    @Override
    public String toString() {
        return "BackButton -> " + super.toString();
    }

}
