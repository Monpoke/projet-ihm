package projetihm.graphics.components.images;

import projetihm.graphics.components.Component;
import projetihm.graphics.components.Panel;
import projetihm.listeners.TouchListener;

import java.awt.*;
import java.awt.event.MouseEvent;

/**
 * ImageBox is a box containing an image. It can be clickable.
 *
 * @author Quentin
 * @version 0.0.0
 * @since 2015-05-26
 */
public class ImageBox extends Panel implements Component {

    /**
     * Image inside the box.
     */
    private Image image;

    /**
     * Whether or not the box has a border.
     */
    private boolean border;

    /**
     * Creates a new image box.
     *
     * @param image Image to put inside the box.
     * @param x X coordinate of the box.
     * @param y Y coordinate of the box.
     * @param width Width of the box.
     * @param height Height of the box.
     */
    public ImageBox(Image image, int x, int y, int width, int height) {
        this(image, x, y, width, height, false, TouchListener.NONE);
    }

    /**
     * Creates a new image box.
     *
     * @param image Image to put inside the box.
     * @param x X coordinate of the box.
     * @param y Y coordinate of the box.
     * @param width Width of the box.
     * @param height Height of the box.
     * @param border Whether or not the box has a border.
     */
    public ImageBox(Image image, int x, int y, int width, int height, boolean border) {
        this(image, x, y, width, height, border, TouchListener.NONE);
    }

    /**
     * Creates a new image box.
     *
     * @param image Image to put inside the box.
     * @param x X coordinate of the box.
     * @param y Y coordinate of the box.
     * @param width Width of the box.
     * @param height Height of the box.
     * @param border Whether or not the box has a border.
     * @param touchListener TouchListener to handle clicks on the image.
     */
    public ImageBox(Image image, int x, int y, int width, int height, boolean border, TouchListener touchListener) {
        super(x, y, width, height);

        this.image = image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        this.border = border;

        addTouchListener(touchListener);
    }

    /**
     * Adds a new touch listener.
     *
     * @param touchListener TouchListener to handle clicks on the image.
     */
    public void addTouchListener(TouchListener touchListener) {
        addMouseListener(touchListener);
    }

    /**
     * Returns the image contained in the box.
     *
     * @return  Image contained in the box.
     */
    public Image getImage() { return image; }

    /**
     * Sets the image to the one specified.
     *
     * @param image Image to display.
     */
    public void setImage(Image image) {
        this.image = image;
    }

    @Override
    public void paintComponent(Graphics g) {
        g.drawImage(image, 0, 0, null);
        if (border) {
            g.setColor(Color.black);
            g.drawRect(0, 0, getWidth() - 1, getHeight() - 1);
        }
    }

    @Override
    public String toString() {
        return "ImageBox [" + getX() + ", " + getY() + ", " + getWidth() + ", " + getHeight() + "] (image " + ((image == null) ? "null" : "not null") + ", " + "border: " + ((border) ? "yes" : "no") + ")";
    }

}
