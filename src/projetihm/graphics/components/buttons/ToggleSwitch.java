package projetihm.graphics.components.buttons;

import projetihm.graphics.components.Component;

import javax.swing.*;
import java.awt.*;

/**
 * ToggleSwitch
 *
 * @author Tititesouris
 * @version 0.0.0
 * @since 2015-06-04
 */
public class ToggleSwitch extends JToggleButton implements Component {

    /**
     * Image to display when the switch is on.
     */
    public static Image onImage;

    /**
     * Image to display when the switch is off.
     */
    public static Image offImage;

    /**
     * Creates a new ToggleSwitch.
     *
     * @param x         X coordinate of the toggle switch.
     * @param y         Y coordinate of the toggle switch.
     * @param width     Width of the toggle switch.
     * @param height    Height of the toggle switch.
     */
    public ToggleSwitch(int x, int y, int width, int height) {
        this(x, y, width, height, false);
    }

    /**
     * Creates a new ToggleSwitch.
     *
     * @param x         X coordinate of the toggle switch.
     * @param y         Y coordinate of the toggle switch.
     * @param width     Width of the toggle switch.
     * @param height    Height of the toggle switch.
     * @param on        Whether or not the switch is on.
     */
    public ToggleSwitch(int x, int y, int width, int height, boolean on) {
        setBounds(x, y, width, height);

        setSelected(on);

        // Necessary to avoid artifacts appearing with the transparency of the image.
        setOpaque(false);
        setBorderPainted(false);

        setVisible(true);
    }

    @Override
    public void update() {

    }

    @Override
    public void paintComponent(Graphics g) {
        if (isSelected()) {
            g.drawImage(onImage.getScaledInstance(getWidth(), getHeight(), Image.SCALE_SMOOTH), 0, 0, null);
        }
        else {
            g.drawImage(offImage.getScaledInstance(getWidth(), getHeight(), Image.SCALE_SMOOTH), 0, 0, null);
        }
    }

    @Override
    public String toString() {
        return "ToggleSwitch [" + getX() + ", " + getY() + ", " + getWidth() + ", " + getHeight() + "] (status: " + ((isSelected()) ? "on" : "off") + ")";
    }

}
