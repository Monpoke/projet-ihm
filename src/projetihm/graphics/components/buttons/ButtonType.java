package projetihm.graphics.components.buttons;


import java.awt.*;

/**
 * ButtonType is an enum representing the different looks a Button can have.
 * @see projetihm.graphics.components.buttons.Button
 *
 * @author Quentin
 * @version 0.0.0
 * @since 2015-05-26
 */
public enum ButtonType {

    /**
     * The default look.
     */
    DEFAULT(new Color(255, 255, 255), new Color(200, 200, 200)),

    /**
     * A green look, useful for a confirm button.
     */
    GREEN(new Color(39, 183, 21), new Color(27, 113, 14)),

    /**
     * A red look, useful for a cancel button.
     */
    RED(new Color(211, 38, 38), new Color(173, 19, 19)),

    /**
     * An orange look, useful for a warning button.
     */
    ORANGE(new Color(227, 140,15), new Color(212, 124, 11));

    /**
     * The color of the button in the normal state.
     */
    private Color normal;

    /**
     * The color of the button in the selected state.
     */
    private Color selected;

    /**
     * Creates a new ButtonType.
     *
     * @param normal    Color of the button in the normal state.
     * @param selected  Color of the button in the selected state.
     */
    ButtonType(Color normal, Color selected) {
        this.normal = normal;
        this.selected = selected;
    }

    /**
     * Returns the color of the button in the normal state.
     *
     * @return  Color of the button in the normal state.
     */
    public Color getDefault() {
        return normal;
    }

    /**
     * Returns the color of the button in the selected state.
     *
     * @return Color of the button in the selected state.
     */
    public Color getSelected() {
        return selected;
    }

}
