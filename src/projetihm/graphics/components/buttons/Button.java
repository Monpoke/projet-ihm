package projetihm.graphics.components.buttons;

import projetihm.graphics.*;
import projetihm.graphics.components.Component;
import projetihm.listeners.TouchListener;

import javax.swing.*;
import java.awt.*;

/**
 * Button is a clickable rectangle that can be selected by adding a TouchListener to it.
 * @see projetihm.listeners.TouchListener
 * @see projetihm.graphics.components.buttons.ButtonType
 *
 * @author Quentin
 * @version 0.0.0
 * @since 2015-05-25
 */
public class Button extends JButton implements Component {

    /**
     * Type of the button. Determines its look.
     */
    private ButtonType type;

    /**
     * Creates a new button.
     *
     * @param label     Text to display on the button.
     * @param x         X coordinate of the button.
     * @param y         Y coordinate of the button.
     * @param width     Width of the button.
     * @param height    Height of the button.
     */
    public Button(String label, int x, int y, int width, int height) {
        this(label, x, y, width, height, Fonts.MEDIUM_FONT, ButtonType.DEFAULT);
    }

    /**
     * Creates a new button.
     *
     * @param label     Text to display on the button.
     * @param x         X coordinate of the button.
     * @param y         Y coordinate of the button.
     * @param width     Width of the button.
     * @param height    Height of the button.
     * @param font      Font of the text to display on the button.
     */
    public Button(String label, int x, int y, int width, int height, Font font) {
        this(label, x, y, width, height, font, ButtonType.DEFAULT);
    }

    /**
     * Creates a new button.
     *
     * @param label     Text to display on the button.
     * @param x         X coordinate of the button.
     * @param y         Y coordinate of the button.
     * @param width     Width of the button.
     * @param height    Height of the button.
     * @param font      Font of the text to display on the button.
     * @param type      Type of the button.
     */
    public Button(String label, int x, int y, int width, int height, Font font, ButtonType type) {
        setBounds(x, y, width, height);

        this.type = type;

        setText(label);
        setForeground(Color.black);
        setBackground(type.getDefault());
        setFont(font);

        setFocusPainted(false);
        setRolloverEnabled(false);
        setBorder(BorderFactory.createLineBorder(Color.black));

        setVisible(true);
    }

    /**
     * Change the button's appearance to make it look selected.
     */
    public void setSelected() {
        setBackground(type.getSelected());
        setBorder(BorderFactory.createLineBorder(Color.black, 2));
    }

    /**
     * Adds a TouchListener to the button, allowing user input to be treated.
     *
     * @param touchListener The touchListener to add.
     */
    public void addTouchListener(TouchListener touchListener) {
        addMouseListener(touchListener);
    }

    @Override
    public void update() {

    }

    @Override
    public String toString() {
        return "Button [" + getX() + ", " + getY() + ", " + getWidth() + ", " + getHeight() + "] (label: \"" + getText() + "\", type: " + type + ")";
    }

}
