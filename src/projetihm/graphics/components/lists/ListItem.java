package projetihm.graphics.components.lists;

import projetihm.graphics.components.Panel;

/**
 * ListItem is an element of a List.
 * @see projetihm.graphics.components.lists.List
 *
 * @author Quentin
 * @version 0.0.0
 * @since 2015-05-26
 */
public abstract class ListItem extends Panel {

    /**
     * Creates a new item.
     *
     * @param x         X coordinate of the item.
     * @param y         Y coordinate of the item.
     * @param width     Width of the item.
     * @param height    Height of the item.
     */
    public ListItem(int x, int y, int width, int height) {
        super(x, y, width, height);
    }

    @Override
    public String toString() {
        return "ListItem [" + getX() + ", " + getY() + ", " + getWidth() + ", " + getHeight() + "]";
    }

}
