package projetihm.graphics.components.lists;

import projetihm.graphics.components.Component;
import projetihm.graphics.components.Panel;

import javax.swing.*;

/**
 * List is a collection of ListItems contained in a ListPanel to allow scrolling.
 * @see projetihm.graphics.components.lists.ListItem
 * @see projetihm.graphics.components.lists.ListPanel
 *
 * @author Quentin
 * @version 0.0.0
 * @since 2015-05-26
 */
public class List extends Panel implements Component {

    /**
     * The list of items.
     */
    protected ListItem[] items;

    /**
     * The speed of scrolling in pixels per update.
     */
    private int scrollSpeed = 7;

    /**
     * The target Y coordinate. If different from the actual Y coordinate, the list is scrolling.
     */
    private int targetY;

    /**
     * Creates a new list.
     *
     * @param items Items in the list.
     */
    public List(ListItem[] items) {
        super(0, 0, items[0].getWidth(), items.length * items[0].getHeight());
        targetY = getY();

        this.items = items;

        // Add all the items one under the other.
        int offset = 0;
        for (int i = 0; i < items.length; i++) {
            items[i].setLocation(0, offset);
            add(items[i]);
            offset += items[i].getHeight();
        }
    }

    /**
     * Scrolls the list by distance specified.
     *
     * @param amount    Distance to scroll. If negative, scrolling downward.
     */
    public void scroll(int amount) {
        int minY = getParent().getHeight() - getHeight();
        targetY = Math.min(0, Math.max(minY, getY() + amount));
    }

    /**
     * Returns the list of all the items.
     *
     * @return  List of the items.
     */
    public ListItem[] getItems() {
        return items;
    }

    @Override
    public void update() {
        if (targetY != getY()) {
            if (Math.abs(getY() - targetY) > scrollSpeed) {
                setLocation(getX(), getY() + scrollSpeed * (int) Math.signum(targetY - getY()));
            } else {
                setLocation(getX(), targetY);
            }
        }
        for (ListItem item : items) {
            item.update();
        }
    }

    @Override
    public String toString() {
        return "List (scrollSpeed: " + scrollSpeed + ", " + items.length + " items)";
    }

}
