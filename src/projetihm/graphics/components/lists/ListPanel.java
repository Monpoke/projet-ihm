package projetihm.graphics.components.lists;

import projetihm.graphics.components.Panel;

/**
 * ListPanel is the container of a List, allowing the list to scroll.
 * @see projetihm.graphics.components.lists.List
 *
 * @author Quentin
 * @version 0.0.0
 * @since 2015-05-29
 */
public class ListPanel extends Panel {

    /**
     * Creates a new list panel.
     *
     * @param x         X coordinate of the panel.
     * @param y         Y coordinate of the panel.
     * @param width     Width of the panel.
     * @param height    Height of the panel.
     * @param list      List contained in the panel.
     */
    public ListPanel(int x, int y, int width, int height, List list) {
        super(x, y, width, height);

        add(list);
    }

    @Override
    public String toString() {
        return "ListPanel [" + getX() + ", " + getY() + ", " + getWidth() + ", " + getHeight() + "]";
    }

}
