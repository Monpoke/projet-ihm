package projetihm.graphics.components.texts;

import projetihm.Constants;
import projetihm.graphics.Fonts;
import projetihm.graphics.components.Component;

import javax.swing.*;
import java.awt.*;

/**
 * Label is a text graphical element contained in a LabelPanel to allow scrolling.
 * @see projetihm.graphics.components.texts.LabelPanel
 *
 * @author Quentin
 * @version 0.0.0
 * @since 2015-05-26
 */
public class Label extends JLabel implements Constants, Component {

    /**
     * The scrolling speed of the text, in pixels per update.
     */
    private int scrollSpeed = 1;

    /**
     * The time to wait before each scrolling, in updates.
     */
    private int waitTime = 500;

    /**
     * Whether or not the text scrolls.
     */
    private boolean scroll;

    /**
     * The target X coordinate. If different from the actual X coordinate, the text is scrolling.
     */
    private int targetX;

    /**
     * The time left to wait before scrolling again in updates.
     */
    private int wait;

    /**
     * Creates a new label.
     *
     * @param text  Text in the label.
     */
    public Label(String text) {
        this(text, Fonts.MEDIUM_FONT, false);
    }

    /**
     * Creates a new Label.
     *
     * @param text  Text in the new label.
     * @param font  Font of the text.
     */
    public Label(String text, Font font) {
        this(text, font, false);
    }

    /**
     * Creates a new Label.
     *
     * @param text      Text in the new label.
     * @param font      Font of the text.
     * @param scroll    Whether or not the label scrolls.
     */
    public Label(String text, Font font, boolean scroll) {
        setBounds(0, 0, getFontMetrics(font).stringWidth(text), getFontMetrics(font).getHeight());

        // Set text
        this.scroll = scroll;
        setForeground(Color.black);
        setFont(font);
        setText(text);

        setVerticalAlignment(CENTER);
        setHorizontalAlignment(CENTER);

        setVisible(true);
    }

    /**
     * Returns whether or not the text scrolls.
     *
     * @return  True if the text scrolls, false otherwise.
     */
    public boolean scrolls() {
        return scroll;
    }

    @Override
    public void update() {
        // If the label scrolls.
        if (scroll) {
            // If it's waiting to scroll.
            if (wait > 0) {
                wait--;
            }
            // If it's scrolling.
            else {
                // If it hasn't reached its target yet.
                if (targetX != getX()) {
                    // If it's not in range of the target yet.
                    if (Math.abs(getX() - targetX) > scrollSpeed) {
                        // Move towards the target at scrollSpeed speed.
                        setLocation(getX() + scrollSpeed * (int) Math.signum(targetX - getX()), getY());
                    }
                    // If it's in range of the target.
                    else {
                        // Move to the target.
                        setLocation(targetX, getY());
                    }
                }
                // If it has reached its target.
                else {
                    // If the target is the left side.
                    if (targetX == 0) {
                        // Change target to the right side.
                        targetX = getParent().getWidth() - getWidth();
                    }
                    // If the target is the right side.
                    else {
                        // Change target to the left side.
                        targetX = 0;
                    }
                    // Wait.
                    wait = waitTime;
                }
            }
        }
    }

    @Override
    public String toString() {
        return "Label (text: \"" + getText() + "\", " + ((scroll) ? "scrollSpeed: " + scrollSpeed + ", waitTime: " + waitTime : "scroll: no") +")";
    }

}
