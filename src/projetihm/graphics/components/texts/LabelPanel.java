package projetihm.graphics.components.texts;

import java.awt.event.MouseListener;
import projetihm.graphics.components.Panel;

/**
 * LabelPanel is a container for Labels, allowing them to scroll.
 * @see projetihm.graphics.components.texts.Label
 *
 * @author Quentin
 * @version 0.0.0
 * @since 2015-05-30
 */
public class LabelPanel extends Panel {
    private final Label label;

    /**
     * Creates a new label panel.
     *
     * @param x         X coordinate of the panel.
     * @param y         Y coordinate of the panel.
     * @param width     Width of the panel.
     * @param height    Height of the panel.
     * @param label     Label contained in the panel.
     */
    public LabelPanel(int x, int y, int width, int height, Label label) {
        super(x, y, width, height);

        this.label = label;
        
        add(label);

        // We set the height of the label so that it is vertically centered in the LabelPanel.
        if (!label.scrolls()) {
            // And if the label doesn't scroll, we resize it to the LabelPanel's size.
            label.setSize(width, height);
        }
        else {
            label.setSize(label.getWidth(), height);
        }
    }
    
    public void setMouseListener(MouseListener ml){
        label.addMouseListener(ml);
    }

    @Override
    public String toString() {
        return "LabelPanel [" + getX() + ", " + getY() + ", " + getWidth() + ", " + getHeight() + "]";
    }

}
