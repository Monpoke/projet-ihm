package projetihm.graphics.components.texts;

import projetihm.graphics.Fonts;
import projetihm.graphics.components.Component;

import javax.swing.*;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * InputBox
 *
 * @author Tititesouris
 * @version 0.0.0
 * @since 2015-06-02
 */
public class InputBox extends JTextField implements Component {

    /**
     * Creates a new input box.
     *
     * @param x         X coordinate of the input box.
     * @param y         Y coordinate of the input box.
     * @param width     Width fo the input box.
     * @param height    Height of the input box.
     */
    public InputBox(int x, int y, int width, int height) {
        this(x, y, width, height, "", Fonts.MEDIUM_FONT);
    }

    /**
     * Creates a new input box.
     *
     * @param x         X coordinate of the input box.
     * @param y         Y coordinate of the input box.
     * @param width     Width fo the input box.
     * @param height    Height of the input box.
     * @param text      Text in the input box.
     */
    public InputBox(int x, int y, int width, int height, String text) {
        this(x, y, width, height, text, Fonts.MEDIUM_FONT);
    }

    /**
     * Creates a new input box.
     *
     * @param x         X coordinate of the input box.
     * @param y         Y coordinate of the input box.
     * @param width     Width fo the input box.
     * @param height    Height of the input box.
     * @param text      Text in the input box.
     * @param font      Font of the text in the input box.
     */
    public InputBox(int x, int y, int width, int height, String text, Font font) {
        setBounds(x, y, width, height);

        setText(text);
        setFont(font);

        setVisible(true);
    }

    @Override
    public void update() {

    }

}
