package projetihm.graphics.components;

import projetihm.Constants;
import projetihm.Debug;
import projetihm.LoadResources;
import projetihm.graphics.screens.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Window is the upper most container. There should never be more than one instance of Window.
 * It contains a single ScreenPanel allowing to switch from one screen to another.
 * @see projetihm.graphics.screens.ScreenPanel
 *
 * @author Quentin
 * @version 0.0.0
 * @since 2015-05-25
 */
public class Window extends JFrame implements Constants {

    /**
     * The update interval in ticks.
     */
    private final int UPDATE_INTERVAL = 10;

    /**
     * The screen panel of the window.
     */
    private final ScreenPanel screenPanel;

    /**
     * Creates a new window.
     */
    public Window() {
        // Apply styles.
        UIManager.getDefaults().put("Button.disabledText", Color.black);
        setCursor(new Cursor(Cursor.HAND_CURSOR));

        // Set the screen panel and load the first screen.
        screenPanel = new ScreenPanel(this, new Register());

        // Set the content pane.
        setContentPane(screenPanel);

        // Need this to work.
        revalidate();

        // Prevent resizing.
        setResizable(false);

        // Set the window to fit the screen.
        pack();

        // Center the window on the screen.
        setLocationRelativeTo(null);

        // Display the window.
        setVisible(true);

        // Allow user to close the window.
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        // Update the app.
        new Timer(UPDATE_INTERVAL, new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                screenPanel.update();
            }
        }
        ).start();

    }

    /**
     * The main class of the program.
     * This is where everything starts.
     *
     * @param args  Arguments passed by the console.
     */
    public static void main(String[] args) {

        // Loading sample resources.
        LoadResources.load();

        // Launch the application.
        Window window = new Window();

    }

    @Override
    public String toString() {
        return "Window [" + getX() + ", " + getY() + ", " + getWidth() + ", " + getHeight() + "]";
    }

}
