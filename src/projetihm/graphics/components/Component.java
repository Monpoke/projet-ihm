package projetihm.graphics.components;

/**
 * Component is an interface that must be implemented by every graphical component to ensure that they update properly.
 *
 * @author Quentin
 * @version 0.0.0
 * @since 2015-05-26
 */
public interface Component {

    /**
     * Updates the component. This method is called every update.
     */
    void update();

}
