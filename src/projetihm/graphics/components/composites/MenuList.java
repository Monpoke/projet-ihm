package projetihm.graphics.components.composites;

import projetihm.graphics.components.lists.List;
import projetihm.graphics.components.lists.ListItem;

import java.awt.*;

/**
 * StudentList is a List containing StudentListItems.
 *
 * @see projetihm.graphics.components.lists.List
 * @see projetihm.graphics.components.composites.StudentListItem
 *
 * @author Quentin
 * @version 0.0.0
 * @since 2015-05-27
 */
public class MenuList extends List {

    /**
     * Creates a new list.
     *
     * @param items Items to fill the list with.
     */
    public MenuList(ListItem[] items) {
        super(items);
    }

    /**
     * Sets every student in the list as present if true of missing if false.
     *
     * @param value Whether the students are present (true) or missing (false).
     */
    public void setStudentsPresent(boolean value) {
        for (ListItem item : items) {
            ((StudentListItem) item).setStudentPresent(value);
        }
    }

    @Override
    public String toString() {
        return "MenuList -> " + super.toString();
    }

}
