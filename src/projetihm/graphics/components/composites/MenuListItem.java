package projetihm.graphics.components.composites;

import projetihm.graphics.components.images.ImageBox;
import projetihm.graphics.components.lists.ListItem;
import projetihm.graphics.components.texts.Label;
import projetihm.graphics.components.texts.LabelPanel;

import java.awt.*;
import java.awt.event.MouseEvent;

import projetihm.graphics.screens.Screen;
import projetihm.listeners.TouchListener;

/**
 * StudentListItem is an item for StudentLists.
 *
 * @see projetihm.graphics.components.composites.StudentList
 *
 * @author Quentin
 * @version 0.0.0
 * @since 2015-05-26
 */
public class MenuListItem extends ListItem {

    /**
     * Height of each item in pixels.
     */
    private static final int ITEM_HEIGHT = 64;

    /**
     * Creates a new item.
     *
     * @param text      Text to display on the item.
     * @param screen    Screen the item redirects to.
     * @param image     Image by the side of the text.
     */
    public MenuListItem(String text, Screen screen, Image image) {
        super(0, 0, SCREEN_WIDTH, ITEM_HEIGHT);

        setBackground(Color.white);

        add(new ImageBox(image, 5, 2, ITEM_HEIGHT - 4, ITEM_HEIGHT - 4));

        LabelPanel labelPanel = new LabelPanel(ITEM_HEIGHT, 0, SCREEN_WIDTH - ITEM_HEIGHT, ITEM_HEIGHT, new Label(text));
        labelPanel.setBackground(Color.white);
        add(labelPanel);

        addMouseListener(new TouchListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
                ((Screen)getParent().getParent().getParent()).changeScreen(screen);
            }
        });

    }

    @Override
    public String toString() {
        return "MenuListItem -> " + super.toString();
    }

}
