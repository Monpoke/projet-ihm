package projetihm.graphics.components.composites;

import projetihm.graphics.Fonts;
import projetihm.graphics.components.Panel;
import projetihm.graphics.components.images.Hamburger;
import projetihm.graphics.components.texts.*;
import projetihm.graphics.components.texts.Label;

import javax.swing.*;
import java.awt.*;
import projetihm.graphics.components.images.BackButton;
import projetihm.graphics.screens.Screen;
import projetihm.graphics.screens.ScreenPanel;
import projetihm.graphics.screens.SideMenu;

/**
 * TopBar is the graphical component appearing at the very top of the app.
 *
 * @see projetihm.graphics.components.images.Hamburger
 *
 * @author Quentin
 * @version 0.0.0
 * @since 2015-05-26
 */
public class TopBar extends Panel {

    /**
     * Creates a new TopBar.
     *
     * @param size Height of the TopBar.
     */
    public TopBar(int size, String text){
        this(size, text, false);
    }
    
    /**
     * Creates a new TopBar.
     *
     * @param size  Height of the TopBar.
     * @param text  Text to display.
     * @param back  Whether the button is a back button (true) or a hamburger.
     */
    public TopBar(int size, String text, boolean back) {
        super(0, 0, SCREEN_WIDTH, size);
        
        // Add the hamburger.
        if (back) {
            add(new BackButton(size - 1));
        } else {
            add(new Hamburger(size - 1));
        }
        
        // Add the scrolling text.
        add(new LabelPanel(size - 1, 0, SCREEN_WIDTH - size + 1, size - 1, new Label(text, Fonts.MEDIUM_FONT, true)));

        // Add a border below to separate the TopBar from the rest of the screen.
        setBorder(BorderFactory.createLineBorder(Color.black));
    }

    @Override
    public String toString() {
        return "TopBar [" + getX() + ", " + getY() + ", " + getWidth() + ", " + getHeight() + "]";
    }

}
