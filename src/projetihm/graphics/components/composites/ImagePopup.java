package projetihm.graphics.components.composites;

import projetihm.LoadResources;
import projetihm.graphics.components.Panel;
import projetihm.graphics.components.images.ImageBox;
import projetihm.graphics.components.lists.*;
import projetihm.listeners.TouchListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * ImagePopup
 *
 * @author Tititesouris
 * @version 0.0.0
 * @since 2015-06-02
 */
public class ImagePopup extends Panel {

    /**
     * List of the icons in the popup.
     */
    private List<ImageBox> icons;

    /**
     * Icon selected.
     */
    private ImageBox selected;

    /**
     * Padding between each element in pixels.
     */
    private static final int PADDING = 10;

    /**
     * Creates a new panel.
     *
     * @param x         X coordinate of the panel.
     * @param y         Y coordinate of the panel.
     * @param width     Width of the panel.
     * @param height    Height of the panel.
     * @param icons     List of the icons to display.
     * @param columns   Number of columns to display.
     */
    public ImagePopup(int x, int y, int width, int height, Image[] icons, int columns) {
        super(x, y, width, height);

        setBackground(Color.white);
        setBorder(BorderFactory.createLineBorder(Color.black));

        this.icons = new ArrayList<>();

        // Set the icon size depending on the number of columns and the size of the popup.
        int iconSize = (width - (columns + 1) * PADDING) / columns;
        // Find the maximum amount of icons that fit in the popup.
        int maxIcons = (width / iconSize) * (height / iconSize);

        // Add all the icons.
        for (int i = 0; i < Math.min(maxIcons, icons.length); i++) {
            ImageBox icon = new ImageBox(icons[i], PADDING + (i % columns) * (PADDING + iconSize), PADDING + (i / columns) * (PADDING + iconSize), iconSize, iconSize, false);
            icon.addTouchListener(new TouchListener() {
                @Override
                public void mouseReleased(MouseEvent e) {
                    selected = icon;
                    setVisible(false);
                }
            });
            this.icons.add(icon);
            add(icon);
        }

        // Hide the popup by default.
        setVisible(false);
    }

    public void addTouchListener(TouchListener touchListener) {
        for (java.awt.Component component : getComponents()) {
            component.addMouseListener(touchListener);
        }
    }

    /**
     * Returns the selected icon.
     *
     * @return  Selected icon.
     */
    public Image getSelected() { return selected.getImage(); }

    /**
     * Selects the icon of id specified.
     *
     * @param icon  Id of the selected icon.
     */
    public void setSelected(int icon) {
        selected = icons.get(icon);
    }

    @Override
    public void update() {
        // To make sure it stays on top of everything.
        repaint();
    }

}
