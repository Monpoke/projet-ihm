package projetihm.graphics.components.composites;

import projetihm.Student;
import projetihm.graphics.Fonts;
import projetihm.graphics.components.checkboxes.*;
import projetihm.graphics.components.checkboxes.Checkbox;
import projetihm.graphics.components.images.ImageBox;
import projetihm.graphics.components.lists.ListItem;
import projetihm.graphics.components.lists.ListPanel;
import projetihm.graphics.components.texts.Label;
import projetihm.graphics.components.texts.LabelPanel;
import projetihm.graphics.screens.Profile;
import projetihm.graphics.screens.Screen;
import projetihm.listeners.SwipeListener;
import projetihm.listeners.TouchListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;

/**
 * StudentListItem is an item for StudentLists.
 * @see projetihm.graphics.components.composites.StudentList
 *
 * @author Quentin
 * @version 0.0.0
 * @since 2015-05-26
 */
public class StudentListItem extends ListItem {

    /**
     * Height of each item in pixels.
     */
    private static final int ITEM_HEIGHT = 60;

    /**
     * Width and height of the profile picture.
     */
    private static final int IMAGE_SIZE = ITEM_HEIGHT - 4;

    /**
     * Text height in pixels.
     */
    private static final int TEXT_HEIGHT = ITEM_HEIGHT - 4;

    /**
     * Left and right margin of the text in pixels.
     */
    private static final int TEXT_MARGIN = 10;

    /**
     * Width and height of the checkbox.
     */
    private static final int CHECKBOX_SIZE = ITEM_HEIGHT - 14;

    /**
     * The student represented in this item.
     */
    private Student student;

    /**
     * The checkbox in this item.
     */
    private Checkbox checkbox;

    /**
     * Creates a new item.
     *
     * @param student Student represented in the item.
     */
    public StudentListItem(Student student) {
        this(student, false);
    }
    /**
     * Creates a new item.
     *
     * @param student   Student represented in the item.
     * @param late      True if the list is to select late students.
     */
    public StudentListItem(Student student, boolean late) {
        super(0, 0, SCREEN_WIDTH, ITEM_HEIGHT);

        setBackground(Color.white);
        setBorder(BorderFactory.createLineBorder(Color.lightGray));

        this.student = student;

        // Add the student's profile picture.
        add(new ImageBox(student.getPhoto(), (ITEM_HEIGHT - IMAGE_SIZE) / 2, (ITEM_HEIGHT - IMAGE_SIZE) / 2, IMAGE_SIZE, IMAGE_SIZE, true));

        // Add the student's name.
        LabelPanel studentName = new LabelPanel(IMAGE_SIZE + TEXT_MARGIN, 2, SCREEN_WIDTH - IMAGE_SIZE - CHECKBOX_SIZE - 2 * TEXT_MARGIN, TEXT_HEIGHT, new Label(student.getName(), Fonts.SMALL_FONT));
        studentName.setBackground(Color.white);
        add(studentName);

        // Add the register checkbox.
        checkbox = new Checkbox(SCREEN_WIDTH - CHECKBOX_SIZE - (ITEM_HEIGHT - CHECKBOX_SIZE) / 2, (ITEM_HEIGHT - CHECKBOX_SIZE) / 2, CHECKBOX_SIZE, CHECKBOX_SIZE, CheckboxType.RED);

        checkbox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (!late) {
                    if (e.getStateChange() == ItemEvent.SELECTED) {
                        student.setPresent(true);
                        checkbox.setType(CheckboxType.GREEN);
                    } else if (e.getStateChange() == ItemEvent.DESELECTED) {
                        student.setPresent(false);
                        checkbox.setType(CheckboxType.RED);
                    }
                } else {
                    if (!student.isPresent()) {
                        if (e.getStateChange() == ItemEvent.SELECTED) {
                            student.setLate(true);
                            checkbox.setType(CheckboxType.ORANGE);
                        } else if (e.getStateChange() == ItemEvent.DESELECTED) {
                            student.setLate(false);
                            checkbox.setType((student.isPresent()) ? CheckboxType.GREEN : CheckboxType.RED);
                        }
                    }
                    else {
                        checkbox.setSelected(true);
                    }
                }
            }
        });
        if (!late) {
            if (student.isRegistered() && student.isPresent()) {
                checkbox.setSelected(true);
            }
        }
        else if (student.isLate() || student.isPresent()) {
            checkbox.setSelected(true);
            if (student.isPresent()) {
                checkbox.setType(CheckboxType.GREEN);
            }
        }
        add(checkbox);
    }

    /**
     * Adds a swipe listener.
     *
     * @param swipeListener Listener to add.
     */
    public void addSwipeListener(SwipeListener swipeListener) {

        addMouseListener(new SwipeListener() {
            @Override
            public void swipeUp(int distance) {
                swipeListener.swipeUp(distance);
            }

            @Override
            public void swipeDown(int distance) {
                swipeListener.swipeDown(distance);
            }

            @Override
            public void swipeLeft(int distance) {
                ((Screen) getParent().getParent().getParent()).changeScreen(new Profile(student));
            }

            @Override
            public void swipeRight(int distance) {
            }
        });
    }

    /**
     * Sets the student present if true or missing if false on the register.
     *
     * @param value Whether the student is present (true) or missing (false).
     */
    public void setStudentPresent(boolean value) {
        student.setPresent(value);
        checkbox.setSelected(value);
        checkbox.setType((value) ? CheckboxType.GREEN : CheckboxType.RED);
    }

    /**
     * Sets the student as late if true or not if false.
     *
     * @param value Whether the student was late (true) or not (false).
     */
    public void setStudentLate(boolean value) {
        if (!student.isPresent()) {
            student.setLate(value);
            checkbox.setSelected(value);
            checkbox.setType((value) ? CheckboxType.ORANGE : CheckboxType.RED);
        }
    }

    @Override
    public String toString() {
        return "StudentListItem -> " + super.toString();
    }

}
