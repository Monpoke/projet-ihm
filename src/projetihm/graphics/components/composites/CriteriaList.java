package projetihm.graphics.components.composites;

import projetihm.graphics.components.lists.List;
import projetihm.graphics.components.lists.ListItem;

/**
 * CriteriaList
 *
 * @author Tititesouris
 * @version 0.0.0
 * @since 2015-06-02
 */
public class CriteriaList extends List {

    /**
     * Creates a new list.
     *
     * @param items Items to fill the list with.
     */
    public CriteriaList(ListItem[] items) {
        super(items);
    }

    @Override
    public String toString() {
        return "CriteriaList -> " + super.toString();
    }

}
