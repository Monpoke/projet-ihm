package projetihm.graphics.components.composites;

import projetihm.Criteria;
import projetihm.graphics.Fonts;
import projetihm.graphics.components.*;
import projetihm.graphics.components.Panel;
import projetihm.graphics.components.images.ImageBox;
import projetihm.graphics.components.lists.ListItem;
import projetihm.graphics.components.texts.*;
import projetihm.graphics.components.texts.Label;
import projetihm.graphics.screens.Screen;
import projetihm.listeners.TouchListener;

import javax.swing.*;
import java.awt.*;
import java.awt.Component;
import java.awt.event.MouseEvent;

/**
 * CriteriaListItem
 *
 * @author Tititesouris
 * @version 0.0.0
 * @since 2015-06-02
 */
public class CriteriaListItem extends ListItem {

    /**
     * Height of each item in pixels.
     */
    private static final int ITEM_HEIGHT = 80;

    /**
     * Width and height of the criteria image.
     */
    private static final int IMAGE_SIZE = ITEM_HEIGHT - 4;

    /**
     * Text height in pixels.
     */
    private static final int TEXT_HEIGHT = ITEM_HEIGHT - 4;

    /**
     * Left and right margin of the text in pixels.
     */
    private static final int TEXT_MARGIN = 10;

    /**
     * The criteria represented in this item.
     */
    private Criteria criteria;

    /**
     * Creates a new item.
     *
     * @param criteria  Criteria represented in the item.
     */
    public CriteriaListItem(Criteria criteria) {
        super(0, 0, SCREEN_WIDTH, ITEM_HEIGHT);

        setBackground(Color.white);
        setBorder(BorderFactory.createLineBorder(Color.lightGray));

        this.criteria = criteria;

        // Add the student's profile picture.
        add(new ImageBox(criteria.getImage(), (ITEM_HEIGHT - IMAGE_SIZE) / 2, (ITEM_HEIGHT - IMAGE_SIZE) / 2, IMAGE_SIZE, IMAGE_SIZE));

        // Add the criteria's name.
        LabelPanel criteriaName = new LabelPanel(IMAGE_SIZE + TEXT_MARGIN, 2, SCREEN_WIDTH - IMAGE_SIZE, TEXT_HEIGHT, new Label(criteria.getName(), Fonts.SMALL_FONT));
        criteriaName.setBackground(Color.white);
        add(criteriaName);

        TouchListener touchListener = new TouchListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
                ((Screen)getParent().getParent().getParent()).changeScreen(new projetihm.graphics.screens.Criteria(criteria));
            }
        };

        addMouseListener(touchListener);
        for (java.awt.Component component : getComponents()) {
            component.addMouseListener(touchListener);
        }

    }

    @Override
    public String toString() {
        return "CriteriatListItem -> " + super.toString();
    }
}
