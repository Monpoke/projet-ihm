package projetihm.graphics.components.checkboxes;

import projetihm.graphics.components.Component;

import javax.swing.*;
import java.awt.*;

/**
 * Checkbox is a toggleable square box.
 * @see projetihm.graphics.components.checkboxes.CheckboxType
 *
 * @author Quentin
 * @version 0.0.0
 * @since 2015-05-26
 */
public class Checkbox extends JCheckBox implements Component {

    /**
     * Image of the tick.
     */
    public static Image tick;

    /**
     * Type of the checkbox. Determines its look.
     */
    private CheckboxType type;

    /**
     * Creates a new checkbox.
     *
     * @param x         X coordinate of the checkbox.
     * @param y         Y coordinate of the checkbox.
     * @param width     Width of the checkbox.
     * @param height    Height of the checkbox.
     */
    public Checkbox(int x, int y, int width, int height) {
        this(x, y, width, height, CheckboxType.DEFAULT);
    }

    /**
     * Creates a new checkbox.
     *
     * @param x         X coordinate of the checkbox.
     * @param y         Y coordinate of the checkbox.
     * @param width     Width of the checkbox.
     * @param height    Height of the checkbox.
     * @param type      Type of the checkbox.
     */
    public Checkbox(int x, int y, int width, int height, CheckboxType type) {
        setBounds(x, y, width, height);

        this.type = type;

        // Necessary to avoid artifacts appearing with the transparency of the image.
        setOpaque(false);

        setVisible(true);
    }

    /**
     * Sets the type of the checkbox to the one specified.
     *
     * @param type  New type of the checkbox.
     */
    public void setType(CheckboxType type) {
        this.type = type;
    }

    @Override
    public void paintComponent(Graphics g) {
        g.drawImage(type.image.getScaledInstance(getWidth(), getHeight(), Image.SCALE_SMOOTH), 0, 0, null);
        if (isSelected()) {
            g.drawImage(tick.getScaledInstance(getWidth(), getHeight(), Image.SCALE_SMOOTH), 0, 0, null);
        }
    }

    @Override
    public void update() {

    }

    @Override
    public String toString() {
        return "Checkbox [" + getX() + ", " + getY() + ", " + getWidth() + ", " + getHeight() + "] (" + type + ", " + ((isSelected()) ? "checked" : "unchecked") + ")";
    }

}
