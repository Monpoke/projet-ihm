package projetihm.graphics.components.checkboxes;

import java.awt.*;

/**
 * CheckboxType is an enum representing the different looks of a checkbox.
 *
 * @author Quentin
 * @version 0.0.0
 * @since 2015-05-27
 */
public enum CheckboxType {

    /**
     * The default look.
     */
    DEFAULT,

    /**
     * A green look, useful for a success checkbox.
     */
    GREEN,

    /**
     * A red look, useful for a danger checkbox.
     */
    RED,

    /**
     * An orange look, useful for a warning checkbox.
     */
    ORANGE;

    /**
     * Image of the checkbox.
     */
    public Image image;

}
