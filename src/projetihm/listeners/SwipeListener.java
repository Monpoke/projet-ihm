package projetihm.listeners;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * SwipeListener is a listener that will trigger whenever the user swiped their finger on the screen.
 *
 * @author Quentin
 * @version 0.0.0
 * @since 2015-05-26
 */
public abstract class SwipeListener implements MouseListener {

    /**
     * Length of a swipe in pixels necessary to trigger the listener.
     */
    private static final int SENSIVITY = 50;

    /**
     * X coordinate before swiping.
     */
    private int x;

    /**
     * Y coordinate before swiping.
     */
    private int y;

    @Override
    public void mousePressed(MouseEvent e) {
        this.x = e.getX();
        this.y = e.getY();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (e.getY() < y && y - e.getY() > SENSIVITY) {
            swipeUp(y - e.getY());
        } else if (e.getY() > y && e.getY() - y > SENSIVITY) {
            swipeDown(e.getY() - y);
        } else if (e.getX() < x && x - e.getX() > SENSIVITY) {
            swipeLeft(x - e.getX());
        } else if (e.getX() > x && e.getX() - x > SENSIVITY) {
            swipeRight(e.getX() - x);
        }
    }

    /**
     * Triggered when the user swiped up.
     *
     * @param distance  Length of the swipe in pixels.
     */
    public abstract void swipeUp(int distance);

    /**
     * Triggered when the user swiped down.
     *
     * @param distance Length of the swipe in pixels.
     */
    public abstract void swipeDown(int distance);

    /**
     * Triggered when the user swiped left.
     *
     * @param distance Length of the swipe in pixels.
     */
    public abstract void swipeLeft(int distance);

    /**
     * Triggered when the user swiped right.
     *
     * @param distance Length of the swipe in pixels.
     */
    public abstract void swipeRight(int distance);

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

}
