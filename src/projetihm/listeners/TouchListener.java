package projetihm.listeners;

import javax.swing.event.MouseInputListener;
import java.awt.event.MouseEvent;

/**
 * TouchListener is a listener triggered whenever the user presses their finger against the screen.
 *
 * @author Quentin
 * @version 0.0.0
 * @since 2015-05-26
 */
public abstract class TouchListener implements MouseInputListener {

    /**
     * An empty touch listener.
     */
    public static final TouchListener NONE = new TouchListener() {
        @Override
        public void mouseReleased(MouseEvent e) {

        }
    };

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public abstract void mouseReleased(MouseEvent e);

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

}
