package projetihm;

import projetihm.graphics.components.Window;

import javax.swing.*;
import java.awt.*;

/**
 * Debug is a utility class facilitating the debugging of the app.
 *
 * @author Quentin
 * @version 0.0.0
 * @since 2015-05-30
 */
public class Debug {

    /**
     * Prints a tree showing the organisation of the components of the app.
     *
     * @param window    The window of the app.
     */
    public static void showTree(Window window) {
        System.out.println(window);
        System.out.println("└─ " + window.getContentPane());
        showTree("    ", (JPanel) window.getContentPane());
    }

    /**
     * Recursive method to print the tree showing the organisation of the components of the app.
     *
     * @param tree  String to print to the left.
     * @param panel Current panel.
     */
    private static void showTree(String tree, JPanel panel) {
        Component[] components = panel.getComponents();
        for (int i = 0; i < components.length; i++) {
            if (i < components.length - 1) {
                System.out.println(tree + "├─ " + components[i]);
            } else {
                System.out.println(tree + "└─ " + components[i]);
            }
            if (components[i] instanceof JPanel) {
                if (i < components.length - 1) {
                    showTree(tree + "│   ", (JPanel) components[i]);
                }
                else {
                    showTree(tree + "    ", (JPanel) components[i]);
                }
            }
        }
    }

}
