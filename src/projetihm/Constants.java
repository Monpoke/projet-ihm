package projetihm;

/**
 * Constants is an interface regrouping all the information shared accross all elements of the app.
 *
 * @author Tititesouris
 * @version 0.0.0
 * @since 2015-05-25
 */
public interface Constants {

    /**
     * Width of the screen
     */
    int SCREEN_WIDTH = 320;

    /**
     * Height of the screen.
     */
    int SCREEN_HEIGHT = 480;

    /**
     * Height of the TopBar.
     */
    int TOPBAR_HEIGHT = 30;

    /**
     * Left switching direction.
     */
    int SWITCH_LEFT = -1;

    /**
     * Right switching direction.
     */
    int SWITCH_RIGHT = 1;

    /**
     * Path to the resources directory.
     */
    String RESOURCES_PATH = "res\\";

    /**
     * Path to the images directory.
     */
    String IMAGES_PATH = RESOURCES_PATH + "images\\";

    /**
     * Path to the assets directory.
     */
    String ASSETS_PATH = RESOURCES_PATH + "assets\\";

    /**
     * Path to the photos directory.
     */
    String PHOTOS_PATH = RESOURCES_PATH + "photos\\";

    /**
     * Path to the criterias directory.
     */
    String CRITERIAS_PATH = IMAGES_PATH + "criterias\\";

    /**
     * Path to the checkboxes directory.
     */
    String CHECKBOXES_PATH = ASSETS_PATH + "checkboxes\\";

}
